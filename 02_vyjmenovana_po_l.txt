Za okny plynulo poklidné odpoledne.
Občas bylo z dáli slyšet hromy.
Začalo se blýskat. 
Babička postavila na stůl hromničku a zapálila ji.
Na čele jí naběhla starostlivá vráska.
Pavlík se ještě nevrátil z lesa, kde sbíral maliny.
Malinko se zastavila.
Nebude plýtvat časem.
Skopla své plyšové bačkory a vydala se k blízkému lesu vstříc tomu malému pacholíkovi.
Zanedlouho doběhla až ke mlýnu, kam obvykle chodila mlít obilí.
Polkla. 
Z divoce rostoucího pelyňku viděla trčet hubené lýtko.
Ale najednou se rostliny zamlely a Pavlík se vynořil s olysalou živou veverkou v ruce.
Zalykal se štěstím. 
